// console.log("Hello")

// Array methods
// JS has built-in funcitons and methods for arrays. This allows us to manipulate and access array items

// Mutator Methods

/*
	-Mutator Methods are functions that "mutate" or change an array after they are created
	-These methods manipulate the original array performing various tasks such as adding and removing elements
*/

	let fruits = ["Apple", "Orange", "Kiwi", "Dragot Fruit"];

	// push()
	/*
		-adds an element in the end of and array AND returns the array's length
		-Syntax
			arrayName.push();
	*/
	console.log("Current Array")
	console.log(fruits); // log fruits array

	let fruitsLength = fruits.push("Mango");//
	console.log(fruitsLength)//5
	console.log("Mutated aeeay from push method:");
	console.log(fruits)//mango is added at the end of the array


	fruits.push("Avocado", "Guava");
	console.log("Muated array from push method");
	console.log(fruits);//avocado and guava added


	// pop()
	/*
		-removes the last element in an array AND returs the removed element
		-syntax:
			arrayName.pop()
	*/


	let removeFruit = fruits.pop();
	console.log(removeFruit); // Guava / shows what fruit was deleted
	console.log("Mutated array from pop method")
	console.log(fruits); // guava is already removed from the array

//////////////////////////////////////////////

// miniactivity - remove the last person in the array
let ghostFighter = ["Eugene", "Dennis", "Alfred", "Taguro"]

function deleteFriend() {
	ghostFighter.pop();
}
deleteFriend();
console.log(ghostFighter)

//////////////////////////////////////////////////

	// unshift()
	/*
		-adds one or more elements at the beginning of an array
		-Syntax:
			arrayName.unshift("elementA");
			arrayName.unshift("elementA", "elementB");
	*/

	fruits.unshift("Lime", "Banana");
	console.log("Mutated Array from unshift method");
	console.log(fruits);

	// shift()
	/*
		-removes an element at the beginning of an array AND returns the removed element
		-Syntax:
			arrayName.shift();
	*/

	let anotherFruit = fruits.shift();
	console.log(anotherFruit);//Lime
	console.log("Mutated array from the shift method")
	console.log(fruits); //lime is removed


	// splice
	/*
		-simultaneously removed elements from a specified index number and adds elements
		-Syntax:
			-arrayName.splice(startingIndex,deleCount,elementsToBeAdded);
	*/

	fruits.splice(1,2,"Lime", "Cherry");
	console.log("Mutated array from splice method");
	console.log(fruits);

	// sort
	/*
		-rearranges the array elements in alphanumeric order
		-syntax:
			arrayName.sort();
	*/

	fruits.sort();
	console.log("Mutated array from sort method");
	console.log(fruits);


	// reverse
	/*
		-reverses the order of array elements
		-Syntax:
			arrayName.reverse();
	*/
	fruits.reverse();
	console.log("Mutated array from reverse method");
	console.log(fruits);




// Non-Mutator Methods
/*
	-are functions taht do not modify or change an array after they are created
	-these methods do not manipulate the original aray performing various tasks such as returning elements from an array and combining arrays and printing the output

*/

let countries = ["US", "PH", "CA", "SG", "TH","FR", "PH", "DE"];
	
	//indexOf
	/*
		-returns the index number of the first matching element found in an array
		-if no match is found, the result is -1
		-the search process will be done from first element proceeding to the last elememt
		-Syntax:
			arrayName/indexOf(searchValue);
			arrayName/indexOf(searchValue, fromIndex);
	*/

	let firstIndex = countries.indexOf('PH', 3);
	console.log("Result of indexOf method " + firstIndex);

	let invalidCountry = countries.indexOf("BR")
	console.log("Result of indexOf method " + invalidCountry); // -1 since "BR" is not in the array

	//lastIndexOf()
	/*
		-returns the index of the last matching element found in an array 
		-the search process will be done from the last element proceeding to the first element
		-Syntax:
			arrayName.lastIndexOf(searchvalue)
			arrayName.lastIndexOf(searchvalue, fromIndex)
	*/

	let lastIndex = countries.lastIndexOf("PH", 3);
	console.log("Result of lastIndexOf method " + lastIndex)

	// slice
	/*
		-portions/slices elemetns from an array AND returns a new array
		-Synrax:
			arrayName.slice(startingIndex)
			arrayName.slice(startingIndex,endingIndex)
	*/

	let slicedArrayA = countries.slice(2)
	console.log("Result from slice method:");
	console.log(slicedArrayA)

	let slicedArrayB = countries.slice(2,4);
	console.log("Result from slice method:");
	console.log(slicedArrayB)

	let slicedArrayC = countries.slice(-4); //starts at the end of the array
	console.log("Result from slice method:");
	console.log(slicedArrayC)

	// toString()
	/*
		-returns an array as a string seperated by commas
		-Syntax:
		arrayName.toString();
	*/

	let stringArray = countries.toString();
	console.log("Result from toString method:");
	console.log(stringArray)

	// concat()
	/*
		-combines two arrays and returns the combined result
		-Syntax: 
			arrayA.concat(arrayB);
			arrayA.concat(elementA)
	*/

	let taskArrayA = ["drink HTML", "eat JS"];
	let taskArrayB = ["inhale css", "breathe sass"];
	let taskArrayC = ["get git", "be node"];

	let tasks = taskArrayA.concat(taskArrayB)
	console.log("Result from concat method: ");
	console.log(tasks)

	// Combine multiple array
	console.log("Result from concat method: ");
	let allTasks = taskArrayA.concat(taskArrayB, taskArrayC);
	console.log(allTasks)

	// Combining arrays with elements
	let combinedTasks = taskArrayA.concat("smell express","throw react");
	console.log("Result from concat method: ");
	console.log(combinedTasks);

	// join()
	/*
		-returns an array as a string seperated by specified sepaator
		-Syntax:
		arrayName.join("separatoString")
	*/
	let users = ["John", "Jane", "Joe", "Robet", "Nej"];
	console.log(users.join())
	console.log(users.join(''))
	console.log(users.join(' - '))



// Iteration methods
/*
	-are loops designed to perform repetitive tasks on arrays
	-loops over all items in an array
	-useful for manipulating array data resulting in complex tasks
*/

	// forEach
	/*
		-similar to a for loop that iterates on each array element
		-for each item in the array, the anonymous function passed in the forEach() method will be run
		-the anonymous function is able to receive the crrent item being iterated or loop over by assigning a parameter
		-variable names for arrays are normally written in the plural form of the data stored in an array
		-it's commom practice to use the singular form of the array content for parameter names used in array loops
		-forEach() does not return anything
		-Syntax:
			arrayName.forEach(function(individualElement){
				statement
				})
	*/

	allTasks.forEach(function(task){
		console.log(task);
	})

	//////////////////////////////////

	// miniactivity -- function that can display the ghostFighters 1 by 1

	function displayOneByOne() {
		ghostFighter.forEach(function(x){
			console.log(x)
		})
	}
	displayOneByOne();


	////////////////////////


	// Using forEach with conditional statements
	// Looping through all away items
	/*
		-it is good practive to ptint the current element in the console hwen working with the array iteration methods to have an ideaa of waht info is being worked on for each iteration of the loop

		-creating a separate variable to store results of an array iteration
		-iteration methods are also good practive to avoid confusion by modifying the original rray

		-mastering loops and arrays allows us developers  to perform a wide range of features that help in data management and analysis

	*/
	let filteredTasks = [];

	allTasks.forEach(function(task){
		console.log(task);

		if (task.length>10) {
			console.log(task)
			filteredTasks.push(task)
		} 
	})
	console.log("Result of filtered tasks: ");
	console.log(filteredTasks);


	// map 
	/*
		-iterates on each element AND returns new array with different values depending on the result of the function's operation
		-Syntax:
			let/const resultArray = arrayName.map(function(individualElement))
	*/

	let numbers = [1,2,3,4,5];

	let numberMap = numbers.map(function(number){
		return number * number;
	})
	console.log("Original Array: ");
	console.log(numbers);
	console.log("Result of map method:");
	console.log(numberMap)

	//map() vs forEach()
	let numberForEach = numbers.forEach(function(number){
		return number * number;
	})
	console.log(numberForEach) // undefined since forEach does not return

	// every()
	/*
		-checks if all elements in an array meet the given conditions
		-this is useful for valdiating data stored in arrays especially when dealing with large amounts of data
		-returns true if all elements meet the condition, otherwise false
		-Syntax:
			let/const resultArray = arrayName.every(function(individualElement){
				return expression/condition
			})
	*/
	let allValid = numbers.every(function(number){
		return (number<3)
	})
	console.log("Result of every method: ");
	console.log(allValid)//false

	// some()
	/*
		-checks if atleast one element in the array meets the given condition
		-returns a true value if atleast one element meets the condition and false if otherwise
		-Syntax:
			let/const resultArray = arrayName.some(function(individualElement){
				return expression/condition
			})
	*/

	let someValid = numbers.some(function(number){
		return (number<2);
	})
	console.log("Result of some method");
	console.log(someValid)


	// combining the returned result from every/some method way may be used in other statements to perdorm consecutive results
	if(someValid) {
		console.log("Some numbers in the array are greater than 2");
	}



/////////////////////////////////////////////////////////////////////
	// filter
	/*
		-returns a new array that contains elements which meets a given condition
		-returns an empty array if no elements are found
		-Syntax:
			let/const resultArray = arrayName.filter(function(indivElemen){
				return expression/condition
			})
	*/

	let filterValid = numbers.filter(function(number){
		return (number<3);
	})
	console.log("Result of filter methods:");
	console.log(filterValid); //[1,2]

	let nothingFound = numbers.filter(function(number){
		return (number = 0)
	})
	console.log("Result");
	console.log(nothingFound) // [] - empty array since none meets the return condtion


///////////////////////////////////////////////////////////////

	// Filterng forEach
	let  filteredNumbers = [];
	numbers.forEach(function(number){
		// console.log(number);

		if (number<3) {
			filteredNumbers.push(number)
		}
	})
	console.log("Result of filter method: ");
	console.log(filteredNumbers);


////////////////////////////////////////////////////////
// includes()
/*
	-checks if the arguments passed can be found in the array
		-it returns a boolen which can be saved in a variable
			-returns true if the argument us found the array
			-returns false if it is not 
	-Syntax:
		arrayName.includes(<argumentToFind)
*/
	let products = ["Mouse", "Keyboard", "Laptop", "Monitor"];

	let productFound = products.includes("Mouse");
	console.log(productFound);

	let productFound2 = products.includes("Headset");
	console.log(productFound2)


////////////////////////////////////////////////////////
//method Chaining
/*
	-methods can be chained using them one after another
	-the result of the first method if used on the second method until all "chained" methods have been resolved
*/

let filteredProducts = products.filter(function(products){
	return products.toLowerCase().includes("a"); // filter all the products that includes "a"
})
console.log(filteredProducts)


////////////////////////
// miniactivity

	let contacts = ["Ash"]


	function addTrainer(trainer){
		let doesTrainerExist = contacts.includes(trainer);

		if (doesTrainerExist) {
			alert("Already Added in the match call")
		} else {
			contacts.push(trainer);
			alert("Registered")
		}
	}


	///////////////////
	//reduce
	/*
		-evaluates elements from left to right and reutrns/reduces the array into a single value
		-Syntax:
			let/const resultArray = arrayName.reduce(function(accumlator, currentValue){
				return expression/operation
			})

			-"Accumulator" paramater in the function stores the result for every iteration of the loop
			-"currentValue" is teh current/next element in the array that is evaluated in each iteration of the loop


			-How the "reduced" method works
			1. the first/result element in the array is stored in the "accumulator" parameter
			2. the second/next element in the array is stored in the currentvalue parameter
			3. an operation is performed on the two elements 
			4. the loop repeats step 1-3 untill all elements
	*/


	console.log(numbers);
	let iteration = 0;
	let iterationStr = 0;

	let reducedArray = numbers.reduce(function(x,y){
		console.warn("current iteration" + ++iteration);
		console.log("accumulator: " + x);
		console.log("current value: " + y);
		return x + y;
	})

	console.log("Result of reduce method: " + reducedArray)//15

	let list = ["Hello", "Again", "World"];
	let reducedJoin = list.reduce(function(x,y){
		console.warn("current iteration" + ++iteration);
		console.log("accumulator: " + x);
		console.log("current value: " + y);
		return x + " " + y;

	})
	console.log("Result of reduced method: " + reducedJoin)

